ARG UPSTREAM
FROM ${UPSTREAM}

COPY ./ /tmp/init/
RUN export PHP_INI_DIR && \
    cd /tmp/init && \
    sh setup.sh && \
    rm -rf /tmp/init

USER totedaten

CMD [ "bash" ]