# PHP Docker Debugging Tools

## ???

The official upstream [PHP Docker Container](https://hub.docker.com/_/php), with extensions and debugging tools.

## WARNING

Since the containers are for local development purposes ONLY, ALL composer plugins are allowed to run.

## Extensions

* ast
* bcmath
* gd
* intl
* ldap
* mysqli
* opcache
* pdo_mysql
* redis
* xdebug
* zip

## Tools and rulesets

* [Composer](https://getcomposer.org/)
* [Disallowed calls for PHPStan](https://github.com/spaze/phpstan-disallowed-calls)
* [Phan](https://github.com/phan/phan)
* [PHP Parallel Lint](https://github.com/php-parallel-lint/PHP-Parallel-Lint)
* [PHP_CodeSniffer](https://github.com/PHPCSStandards/PHP_CodeSniffer/)
* [PHPDoc Parser for PHPStan](https://github.com/phpstan/phpdoc-parser)
* [phpDocumentor](https://www.phpdoc.org/)
* [PHPMD](https://phpmd.org/)
* [PHPStan deprecation rules](https://github.com/phpstan/phpstan-deprecation-rules)
* [PHPStan PHPUnit extensions and rules](https://github.com/phpstan/phpstan-phpunit)
* [PHPStan](https://phpstan.org/)
* [PHPUnit](https://phpunit.de/)
* [Slevomat Coding Standard](https://github.com/slevomat/coding-standard)
* [PHP Coding Standards Fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer)


## Other Infos

Xdebug mode is set to "debug", "coverage" and "develop". Default shell is bash running as totedaten user.

## Tags

### PHP 7.4

* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.16/7.4
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bullseye/7.4

### PHP 8.0

* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.16/8.0
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bullseye/8.0

### PHP 8.1

* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bullseye/8.1
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bookworm/8.1
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.18/8.1
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.19/8.1

### PHP 8.2

* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bullseye/8.2
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bookworm/8.2
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.18/8.2
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.19/8.2

### PHP 8.3

* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bullseye/8.3
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/bookworm/8.3
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.18/8.3
* registry.gitlab.com/teigtascherlfabrik/php-docker-debugging-tools/alpine3.19/8.3