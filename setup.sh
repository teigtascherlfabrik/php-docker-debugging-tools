#!/bin/sh

set -x

deps="bash wget sudo"

_pkg=$( command -v "apk" )
if [ ${?} -eq 0 ]; then
    apk add ${deps}
    rm -rf /var/lib/apk
    adduser totedaten
else
    apt-get update
    apt-get install --yes ${deps}
    rm -rf /var/lib/apt
    useradd --create-home totedaten
fi

set -eux

echo "totedaten ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/totedaten

wget --no-verbose --directory-prefix=/tmp/downloads --input-file=phpreqs.txt
chmod +x /tmp/downloads/*

for file in /tmp/downloads/*; do
    newname=$(basename "${file}" .phar | tr '[:upper:]' '[:lower:]')
    mv "${file}" "/usr/local/bin/${newname}"
    echo "${newname} --version" >> /home/totedaten/.bashrc
done

rm -rf /tmp/downloads

install-php-extensions \
    bcmath \
    ldap \
    zip \
    pdo_mysql \
    mysqli \
    gd \
    redis \
    opcache\
    ast \
    intl \
    xdebug \
    @composer

sudo -u totedaten composer global config --no-plugins allow-plugins true

sudo -u totedaten composer global require \
    phpstan/phpdoc-parser \
    phpstan/phpstan-phpunit \
    phpstan/phpstan-deprecation-rules \
    slevomat/coding-standard \
    spaze/phpstan-disallowed-calls

sudo -u totedaten composer clearcache

mv 99-xdebug.ini ${PHP_INI_DIR}/conf.d/99-xdebug.ini